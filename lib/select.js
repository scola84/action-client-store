'use strict';

const Diff = require('@scola/diff');
const Action = require('@scola/action-client');

class SelectAction extends Action.Abstract {
  constructor(validator, cache) {
    super(validator);

    this.properties.cache = cache;
    this.bindListeners();
  }

  destroy() {
    this.unbindListeners();
    return super.destroy();
  }

  setName(name) {
    super.setName(name);
    this.properties.cache.setHash(this.getHash());

    return this;
  }

  setData(data) {
    super.setData(data);
    this.properties.cache.setHash(this.getHash());

    return this;
  }

  cache() {
    return this.properties.cache;
  }

  bindListeners() {
    this.proxyListener('change', this.properties.cache);
  }

  unbindListeners() {
    this.unproxyListener('change', this.properties.cache);
  }

  handleResponse(response, request, message) {
    this.messenger.emit('debug', this, 'handleResponse',
      response, request, message);

    if (response.data.error) {
      return super.handleResponse(response, request, message);
    }

    if (response.type === 'delete') {
      return this.properties.cache
        .deleteAll()
        .then(super.handleResponse.bind(this, response, request, message));
    }

    if (response.diff) {
      return this.properties.cache
        .getAll()
        .then(this.handleDiff.bind(this, response))
        .then(this.handleData.bind(this, response, request, message));
    }

    return this.handleData(response, request, message);
  }

  handleDiff(response, cachedData) {
    this.messenger.emit('debug', this, 'handleDiff', response, cachedData);
    response.data = Diff.transform(cachedData, response.diff);
  }

  handleData(response, request, message) {
    this.messenger.emit('debug', this, 'handleData',
      response, request, message);

    return this.properties.cache
      .setAll(response.data)
      .then(super.handleResponse.bind(this, response, request, message));
  }
}

module.exports = SelectAction;
