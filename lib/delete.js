'use strict';

const Action = require('@scola/action-client');

class DeleteAction extends Action.Abstract {}

module.exports = DeleteAction;
