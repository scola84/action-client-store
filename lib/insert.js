'use strict';

const Action = require('@scola/action-client');

class InsertAction extends Action.Abstract {}

module.exports = InsertAction;
