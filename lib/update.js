'use strict';

const Action = require('@scola/action-client');

class UpdateAction extends Action.Abstract {}

module.exports = UpdateAction;
