module.exports = {
  Delete: require('./lib/delete.js'),
  Insert: require('./lib/insert.js'),
  Select: require('./lib/select.js'),
  Update: require('./lib/update.js')
};
